import 'package:demoecommerce/commons/loading.dart';
import 'package:demoecommerce/home.dart';
import 'package:demoecommerce/pages/signup.dart';
import 'package:flutter/material.dart';
import './pages/login.dart';
import './provider/user_provider.dart';
import 'package:provider/provider.dart';
import './pages/Splash.dart';

void main(){
  runApp(MultiProvider(providers: [
                         ChangeNotifierProvider(create: (_) => UserProvider.instance()),
                         
  ],
    
      child:     MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Colors.red.shade900
      ),
        home: ScreenController(),
        
      // home: HomePage(),
    )
    )
    
  );

}

class ScreenController extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user= Provider.of<UserProvider>(context);
      //user.signOut();
      print("eel estatus es ${user.status}");
    switch(user.status) 
    { 
      case Status.Unauthenticated: return Login();
      case Status.Uninitialiazed :  return Splash();
      case Status.Authenticating:return Login();
      case Status.Authenticated: return HomePage();
     default: return Login();
    }
    
  }
}

//me quede en el video 4 minuto 0:00